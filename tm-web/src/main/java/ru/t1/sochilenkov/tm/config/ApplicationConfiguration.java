package ru.t1.sochilenkov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.sochilenkov.tm")
public class ApplicationConfiguration {
}
