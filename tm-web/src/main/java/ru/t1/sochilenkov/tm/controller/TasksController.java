package ru.t1.sochilenkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.TaskRepository;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;


    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskRepository.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}
